<?php
/* @var $this GroupChatController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Group Chats',
);

$this->menu=array(
	array('label'=>'Create GroupChat', 'url'=>array('create')),
	array('label'=>'Manage GroupChat', 'url'=>array('admin')),
);
?>

<h1>Group Chats</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
