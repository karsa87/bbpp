<?php
/* @var $this ContactsController */
/* @var $model Contacts */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contacts-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama'); ?>
		<?php echo $form->textField($model,'nama',array('size'=>32,'maxlength'=>120)); ?>
		<?php echo $form->error($model,'nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipe'); ?>
		<?php 
			echo $form->dropDownList($model,'tipe', 
				array(
					'purnawidya'=>'Purnawidya'
					, 'widyaswara'=>'Widyaswara'
				)
			); 
		?>
		<?php echo $form->error($model,'tipe'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'instansi'); ?>
		<?php echo $form->textField($model,'instansi',array('size'=>32,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'instansi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telepon1'); ?>
		<?php echo $form->textField($model,'telepon1',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'telepon1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telepon2'); ?>
		<?php echo $form->textField($model,'telepon2',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'telepon2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email1'); ?>
		<?php echo $form->textField($model,'email1',array('size'=>32,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'email1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email2'); ?>
		<?php echo $form->textField($model,'email2',array('size'=>32,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'email2'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->