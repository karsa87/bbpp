<?php
/* @var $this GroupController */
/* @var $model Group */

$this->breadcrumbs=array(
	'Groups'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Group', 'url'=>array('index')),
	array('label'=>'Create Group', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
var old_id;

window.show_chat = function (clicked_id)
{
	var div_display = 'chat-form'+'-'+old_id;
	var elems = document.getElementsByClassName(div_display);
	for (i = 0; i < elems.length; i++) {
	    elems[i].style.display = 'none';
	    elems[i].style.top = '100%';
	}

	old_id = clicked_id;
    $('.chat-form'+'-'+clicked_id).toggle();
	return false;
};
");

Yii::app()->clientScript->registerCss('style',"
		div#chat-form {
		    background: #eee none repeat scroll 0 0;
		    margin: 10px 0;
		    padding: 10px;
		}
	");
?>

<div class="chat">
	<div class="group_chat" style="width: 30%; float: left;">
		<ul>
			<?php
				$count = count($model);
				
				for ($i=0; $i < $count; $i++) { 
					?>
					<li>
						<?php 
							echo CHtml::link($model[$i]->nama, array('groupChat/chathistory', 'group_id'=>$model[$i]->id));

							// echo CHtml::link($model[$i]->nama,'#',
							// 	array(
							// 		'class'=>'group-button'
							// 		, 'id'=>$model[$i]->id
							// 		// , 'onclick'=>'show_chat(this.id)'


							// 	)
							// );
						?>
						<div class="chat-form<?php echo "-".$model[$i]->id;?>" id="chat-form" style="display:none; width: 65%; float: right" >
							<?php $this->renderPartial('_chatHistory',array(
								'model'=>$model,
								'id'=>$model[$i]->id
							)); ?>
						</div>
					</li>
					<?php
				}
			?>
		</ul>
	</div>


</div>
