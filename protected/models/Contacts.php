<?php

/**
 * This is the model class for table "{{contact}}".
 *
 * The followings are the available columns in table '{{contact}}':
 * @property integer $id
 * @property string $nama
 * @property string $tipe
 * @property string $instansi
 * @property string $telepon1
 * @property string $telepon2
 * @property string $email1
 * @property string $email2
 *
 * The followings are the available model relations:
 * @property Group[] $groups
 * @property GroupChat[] $groupChats
 * @property GroupContact[] $groupContacts
 * @property User[] $users
 */
class Contacts extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{contact}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, tipe, telepon1, email1', 'required'),
			array('nama', 'length', 'max'=>120),
			array('tipe', 'length', 'max'=>20),
			array('instansi, email1, email2', 'length', 'max'=>60),
			array('telepon1, telepon2', 'length', 'max'=>32),
			array('email1, email2', 'email'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, tipe, instansi, telepon1, telepon2, email1, email2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'groups' => array(self::HAS_MANY, 'Group', 'contact_id'),
			'groupChats' => array(self::HAS_MANY, 'GroupChat', 'contact_id'),
			'groupContacts' => array(self::HAS_MANY, 'GroupContact', 'contact_id'),
			'users' => array(self::HAS_MANY, 'User', 'contact_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'tipe' => 'Tipe',
			'instansi' => 'Instansi',
			'telepon1' => 'Telepon 1',
			'telepon2' => 'Telepon 2',
			'email1' => 'Email 1',
			'email2' => 'Email 2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tipe',$this->tipe,true);
		$criteria->compare('instansi',$this->instansi,true);
		$criteria->compare('telepon1',$this->telepon1,true);
		$criteria->compare('telepon2',$this->telepon2,true);
		$criteria->compare('email1',$this->email1,true);
		$criteria->compare('email2',$this->email2,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contacts the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
