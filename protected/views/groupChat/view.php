<?php
/* @var $this GroupChatController */
/* @var $model GroupChat */

$this->breadcrumbs=array(
	'Group Chats'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List GroupChat', 'url'=>array('index')),
	array('label'=>'Create GroupChat', 'url'=>array('create')),
	array('label'=>'Update GroupChat', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GroupChat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GroupChat', 'url'=>array('admin')),
);
?>

<h1>View GroupChat #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'contact_id',
		'group_id',
		'pesan',
		'source',
		'created',
	),
)); ?>
