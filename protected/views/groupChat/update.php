<?php
/* @var $this GroupChatController */
/* @var $model GroupChat */

$this->breadcrumbs=array(
	'Group Chats'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GroupChat', 'url'=>array('index')),
	array('label'=>'Create GroupChat', 'url'=>array('create')),
	array('label'=>'View GroupChat', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GroupChat', 'url'=>array('admin')),
);
?>

<h1>Update GroupChat <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>