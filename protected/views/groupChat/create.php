<?php
/* @var $this GroupChatController */
/* @var $model GroupChat */

$this->breadcrumbs=array(
	'Group Chats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GroupChat', 'url'=>array('index')),
	array('label'=>'Manage GroupChat', 'url'=>array('admin')),
);
?>

<h1>Create GroupChat</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>