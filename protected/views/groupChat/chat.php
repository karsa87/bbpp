<?php
/* @var $this GroupChatController */
/* @var $newmodel GroupChat */

$this->breadcrumbs=array(
	'Group Chats'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List GroupChat', 'url'=>array('index')),
	array('label'=>'Create GroupChat', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "");

Yii::app()->clientScript->registerCss('mycss', "
     #chats
     {
       	width: 100%;
		height: 500px;
		overflow: scroll;
     }

     ul.chats
     {
     	list-style:none;
     }
");
?>

<h1>Chats</h1>
<?php echo CHtml::button('Back',array('onclick'=>'js:history.go(-1);returnFalse;','style'=>'font-size: 14px;font-weight: bold;')); ?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'group-chat-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($newmodel); ?>

	<div class="portlet">
		<div class="portlet-body" id="chats">
			<div class="scroller" style="height:435px" data-always-visible="1" data-rail-visible1="1">
				<ul class="chats">
				<?php 
					for ($i=0; $i < count($model); $i++) { 
						$newDate = date("d-m-Y H:i", strtotime($model[$i]->created));
						echo "<li>";
							echo "<div class = 'message'>";
								echo "<span class='name_user'><b>".$model[$i]->contact->nama."</b></span>";
								echo "<span class='datetime'> at $newDate</span></br>";
								echo "<span class='body'> ".$model[$i]->pesan."</span>";
							echo "</div>";
						echo "</li>";
					}
				?>
				</ul>
			</div>
		</div>
		<div class="chat-form">
				<div class="row-fluid">
					<div class="span7">
						<div class="input-cont">
							<div class="row">
								<?php echo $form->textArea($newmodel,'pesan',array('rows'=>6, 'cols'=>50)); ?>
								<?php echo $form->error($newmodel,'pesan'); ?>
							</div>
							<!-- <input class="m-wrap" type="text" placeholder="Type a message here..." /> -->
						</div>
					</div>
					<div class="span5">
						
						<div class="input-cont-btn">
							<?php echo CHtml::submitButton('Send'); ?>
							<a class="btn blue icn-only" href="#">add</a>
							<a class="btn blue icn-only" href="#">sms</a>
							<a class="btn blue icn-only" href="#">email</a>
						</div>
					</div>
				</div>
			</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->